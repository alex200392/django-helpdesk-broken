FROM python:3.8

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

COPY ./ /django-helpdesk
WORKDIR /django-helpdesk

# RUN apt-get -y install make
RUN pip install --upgrade pip && pip install -r requirements.txt
RUN pip install django-createsuperuserwithpassword
RUN pip install django-helpdesk
RUN pip install psycopg2
RUN pip install djangorestframework
RUN pip install django-filter

RUN pip install -e .
RUN pip install -e demo

RUN find . | grep -E "(__pycache__|\.pyc|\.pyo$)" | xargs rm -rf

EXPOSE 8000

HEALTHCHECK --interval=10s --timeout=10s --retries=3 CMD curl --fail http://localhost:8000/  || exit 1

ENTRYPOINT [ "bash", "entrypoint.sh" ]
