#!/bin/sh

demodesk makemigrations
demodesk migrate --noinput
demodesk createsuperuserwithpassword --username ${DJANGO_SUPERUSER_NAME} --password ${DJANGO_SUPERUSER_PASSWORD} --email admin@example.org --preserve
demodesk loaddata emailtemplate.json
demodesk loaddata demo.json
demodesk loaddata fixture.json
demodesk runserver 0.0.0.0:8000
